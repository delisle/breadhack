# breadhack

This is a hack of a bread machine to control it with a Teensy 3.1 board.
The Teensy board controls the bread machine through two analog switches (HEF4016B) soldered on the original buttons (see demo).

## License

GPLv3
