// Copyright 2022 Jean-Baptiste Delisle
//
// This file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this file.  If not, see <http://www.gnu.org/licenses/>.

// Bread machine buttons
#define MENU_PIN 14
#define LOAF_PIN 21
#define MINUS_PIN 20
#define PLUS_PIN 9
#define COLOR_PIN 8
#define START_PIN 7

// Teensy buttons
#define N_BUT 4
const int BUT_PINS[] = {15, 16, 17, 18};
// Number of values for initial computation of mean capacity
#define MEAN_NVAL 500
// Coef of softening for refreshing buttons mean capacity
#define MEAN_BUT_SOFT_COEF 0.001
// Minimum capacity (above mean value) for activating buttons
#define BUT_THRESHOLD 500
// Mean value of buttons capacity (when not touched)
float mean_but[] = {0.0, 0.0, 0.0, 0.0};
// Fingering (combination of buttons) to stop the current recipe
#define FING_STOP 8

elapsedMillis elapsed = 0;
int stopping = 0;

// Compute mean value (at rest) of buttons capacity
void compute_mean_capacities()
{
  int but, k;
  for (but = 0; but < N_BUT; but++) {
    mean_but[but] = 0.0;
    for (k = 0; k < MEAN_NVAL; k++) {
      mean_but[but] += touchRead(BUT_PINS[but]);
    }
    mean_but[but] /= MEAN_NVAL;
  }
}

// Read buttons values
int read_buts()
{
  int but;
  int fing = 0;
  float pinread;
  for (but = 0; but < N_BUT; but++) {
    fing <<= 1; // (* 2)
    pinread = touchRead(BUT_PINS[but]);
    pinread -= mean_but[but];
    if (pinread > BUT_THRESHOLD) {
      fing += 1;
    } else {
      mean_but[but] += MEAN_BUT_SOFT_COEF * pinread;
    }
  }
  return (fing);
}

// Check if the user pushed the stop button
int stop()
{
  if (!stopping) {
    int fing = read_buts();
    stopping = (fing == FING_STOP);
  }
  return (stopping);
}

// Emulate a short button push on the bread machine
void push(int pin)
{
  digitalWrite(pin, HIGH);
  delay(200);
  digitalWrite(pin, LOW);
  delay(200);
}

// Emulate several short button pushes on the bread machine
void npush(int pin, int count)
{
  for (int i = 0; i < count; i++) {
    if (stop())
      return;
    push(pin);
  }
}

// Emulate a long button push on the bread machine
void longpush(int pin)
{
  digitalWrite(pin, HIGH);
  delay(3000);
  digitalWrite(pin, LOW);
  delay(200);
}

// Wait h hour
void wait_hour(float h)
{
  uint millis = (uint)(h * 3600000);
  elapsed = 0;
  while (elapsed < millis) {
    if (stop())
      return;
  }
}

// Custom recipes
void recipe_leaven_bread()
{
  npush(MENU_PIN, 2);
  if (stop())
    return;
  push(START_PIN);
  if (stop())
    return;
  wait_hour(4.8334);
  if (stop())
    return;
  longpush(START_PIN);
  if (stop())
    return;
  npush(MENU_PIN, 13);
  if (stop())
    return;
  push(MINUS_PIN);
  if (stop())
    return;
  push(START_PIN);
  if (stop())
    return;
  push(COLOR_PIN);
  if (stop())
    return;
  push(START_PIN);
}

void recipe_leaven_brioche()
{
  npush(MENU_PIN, 2);
  if (stop())
    return;
  push(START_PIN);
}

void recipe_pizza()
{
  npush(MENU_PIN, 11);
  if (stop())
    return;
  push(START_PIN);
}

// The setup() method runs once, when the sketch starts
void setup()
{
  pinMode(MENU_PIN, OUTPUT);
  pinMode(PLUS_PIN, OUTPUT);
  pinMode(MINUS_PIN, OUTPUT);
  pinMode(LOAF_PIN, OUTPUT);
  pinMode(COLOR_PIN, OUTPUT);
  pinMode(START_PIN, OUTPUT);
  compute_mean_capacities();
  // wait for the bread machine to be ready
  delay(3000);
}

// the loop() method runs over and over again,
void loop()
{
  stopping = 0;
  // Read the fingering (combination of capacity buttons)
  int fing = read_buts();
  switch (fing) {
  case 1:
    recipe_leaven_bread();
    break;
  case 2:
    recipe_leaven_brioche();
    break;
  case 4:
    recipe_pizza();
    break;
  }
}
